# Marvel Unlimited: Dracula Discovery

## _Shane Stafford, developer_

## Description
This app allows users to discover unique Dracula-related comics found in the Marvel Universe. Users can easily find a random Dracula comic, view its details and then save it to their personal reading list.

## Getting Started
### Technologies Used
* [Android SDK 21+](https://developer.android.com/docs)
* [Kotlin](https://kotlinlang.org/docs/reference/)
* [Koin](https://insert-koin.io/)
* [Coroutines](https://kotlinlang.org/docs/reference/coroutines-overview.html)
* [OkHttp](https://square.github.io/okhttp/)
* [Picasso](https://square.github.io/picasso/)
* [Room](https://developer.android.com/topic/libraries/architecture/room)
* [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel)
* [Espresso](https://developer.android.com/training/testing/espresso/basics)

### Development Prerequisites
* [Git](https://git-scm.com/)
* [Android Studio](https://developer.android.com/studio/index.html)
* [Marvel API Developer Account](https://developer.marvel.com/)

### Installation
* `git clone https://gitlab.com/darkknightsds/marvel-unlimited-dracula-discovery`
* `cd marvel-unlimited-dracula-discovery`
* `touch apikey.properties`
* Inside of `apikey.properties`, add two lines:
  * MARVEL_PUB="your_public_api_key"
  * MARVEL_PRIV="your_private_api_key"

### Running the App
* Open Android Studio
* Choose "Open existing project"
* Select `marvel-unlimited-dracula-discovery` directory
* Sync Gradle/ build project
* Press green arrow ("Run") to install APK

## App Screenshot
*Discover new comics, view details and save items for future offline reference*

![Example Screenshot](https://gitlab.com/darkknightsds/marvel-unlimited-dracula-discovery/uploads/85dda4c6469e58d3b1cf8e60c096d721/Screenshot_20200121-122321.png)

## Licensing
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
