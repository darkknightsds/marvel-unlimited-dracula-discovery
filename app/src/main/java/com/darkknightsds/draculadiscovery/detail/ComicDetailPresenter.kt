package com.darkknightsds.draculadiscovery.detail

import android.content.Context
import com.darkknightsds.draculadiscovery.comic.Comic
import com.darkknightsds.draculadiscovery.comic.ComicDatabase

class ComicDetailPresenter(val context: Context) {
    fun saveComic(comic: Comic) {
        val db = ComicDatabase.getDatabase(context)

        db.comicDao().insert(comic)
    }

    fun deleteComic(comic: Comic) {
        val db = ComicDatabase.getDatabase(context)

        db.comicDao().delete(comic)
    }
}