package com.darkknightsds.draculadiscovery.detail

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.ViewModelProviders

import com.darkknightsds.draculadiscovery.R
import com.darkknightsds.draculadiscovery.comic.Comic
import com.darkknightsds.draculadiscovery.comic.ComicStatus
import com.darkknightsds.draculadiscovery.readinglist.ReadingListViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_comic_detail.*
import kotlinx.coroutines.*
import org.koin.android.ext.android.inject
import java.lang.Exception

class ComicDetailFragment(private val comic: Comic, private val status: ComicStatus) : Fragment(), View.OnClickListener {
    companion object {
        val parentJob = Job()
    }

    private val comicDetailPresenter: ComicDetailPresenter by inject()

    private val coroutineScope = CoroutineScope(Dispatchers.Main + parentJob)

    private lateinit var viewModel: ReadingListViewModel

    private lateinit var changedStatus: ComicStatus

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_comic_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        progressBar_comicLoading.visibility = View.VISIBLE

        val marvelTypeface = ResourcesCompat.getFont(
            context!!,
            R.font.marvel_font
        )

        val robotoTypeface = ResourcesCompat.getFont(
            context!!,
            R.font.roboto
        )

        changedStatus = status

        updateButtonText()

        button_readingList.setOnClickListener(this)
        button_readingList.typeface = marvelTypeface

        textView_title.isAllCaps = true
        textView_title.typeface = marvelTypeface

        textView_description.typeface = robotoTypeface

        displayImage()

        viewModel = activity?.run {
            ViewModelProviders.of(this).get(ReadingListViewModel::class.java)
        } ?: throw Exception()
    }

    private fun displayImage() {
        Picasso.get().load(comic.image).fit().centerCrop().into(imageView_comicImage,
            object : com.squareup.picasso.Callback {
                override fun onSuccess() {
                    progressBar_comicLoading.visibility = View.GONE
                    displayText()
                }

                override fun onError(e: Exception?) {
                }
            })
    }

    private fun displayText() {
        if (comic.title != "null") {
            textView_title.text = comic.title
        } else {
            textView_title.text = getString(R.string.no_title_provided)
        }
        if (comic.description != "null") {
            textView_description.text = comic.description
        } else {
            textView_description.text = getString(R.string.no_description_provided)
        }
        textView_description.movementMethod = ScrollingMovementMethod()
        button_readingList.visibility = View.VISIBLE
    }

    override fun onClick(v: View?) {
        when (v) {
            button_readingList -> {
                if (changedStatus == ComicStatus.RANDOM) {
                    changeStatus(ComicStatus.READING_LIST)
                    updateButtonText()
                    saveToReadingList()
                } else {
                    changeStatus(ComicStatus.RANDOM)
                    updateButtonText()
                    removeFromReadingList()
                }
            }
        }
    }

    private fun changeStatus(status: ComicStatus) {
        changedStatus = status
    }

    private fun updateButtonText() {
        if (changedStatus == ComicStatus.RANDOM) {
            button_readingList.text = resources.getString(R.string.add_to_reading_list)
        } else {
            button_readingList.text = resources.getString(R.string.remove_from_reading_list)
        }
    }

    private fun saveToReadingList() {
        coroutineScope.launch(Dispatchers.IO) {
            val update = async { comicDetailPresenter.saveComic(comic) }
            update.await()
            viewModel.readingListChanged()
        }
    }

    private fun removeFromReadingList() {
        coroutineScope.launch(Dispatchers.IO) {
            val update = async { comicDetailPresenter.deleteComic(comic) }
            update.await()
            viewModel.readingListChanged()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        coroutineScope.coroutineContext.cancelChildren()
    }
}
