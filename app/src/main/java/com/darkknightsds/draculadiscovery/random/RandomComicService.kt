package com.darkknightsds.draculadiscovery.random

import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException

class RandomComicService {
    var client = OkHttpClient()

    fun getComics(url: HttpUrl): String? {
        val request: Request = Request.Builder()
            .url(url)
            .build()

        client.newCall(request).execute().use { response ->
            if (!response.isSuccessful) throw IOException("Unexpected code $response")

           return response.body?.string()
        }
    }
}