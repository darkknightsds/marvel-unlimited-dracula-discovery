package com.darkknightsds.draculadiscovery.random

import com.darkknightsds.draculadiscovery.BuildConfig
import com.darkknightsds.draculadiscovery.comic.Comic
import okhttp3.HttpUrl
import okhttp3.internal.and
import org.json.JSONObject
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import kotlin.random.Random

class RandomComicPresenter: KoinComponent {
    private val randomComicService: RandomComicService by inject()

    fun getRandomComic(): Comic {
        val data = getData()
        val comicsList = getRandom(data)

        return generateRandomComic(comicsList)
    }

    private fun getData(): String? {
        val time = System.currentTimeMillis().toString()

        val url = HttpUrl.Builder()
            .scheme("http")
            .host("gateway.marvel.com")
            .addPathSegment("v1")
            .addPathSegment("public")
            .addPathSegment("characters")
            .addPathSegment("1010677")
            .addPathSegment("comics")
            .addQueryParameter("limit", "40")
            .addQueryParameter("ts", time)
            .addQueryParameter("apikey", BuildConfig.MARVEL_PUB)
            .addQueryParameter("hash", md5(time+BuildConfig.MARVEL_PRIV+BuildConfig.MARVEL_PUB))
            .build()

        return randomComicService.getComics(url)
    }

    private fun getRandom(data: String?): JSONObject {
        val results = JSONObject(data).getJSONObject("data").getJSONArray("results")

        val randomIndex = Random.nextInt(0, results.length())

        return results.getJSONObject(randomIndex)
    }

    private fun generateRandomComic(obj: JSONObject): Comic {
        val id = obj.getInt("id")
        val title = obj.getString("title")
        val description = obj.getString("description")
        val image = obj.getJSONArray("images").getJSONObject(0)

        val path = image.getString("path")
        val extension = image.getString("extension")
        val thumbnail = "$path/standard_xlarge.$extension"
        val comicImage = "$path/portrait_uncanny.$extension"

        return Comic(
            id,
            title,
            description,
            thumbnail,
            comicImage,
            System.currentTimeMillis()
        )
    }

    private fun md5(hashArgs: String): String? {
        try {
            val md = MessageDigest.getInstance("MD5")
            val array = md.digest(hashArgs.toByteArray())
            val sb = StringBuffer()
            for (i in array.indices) {
                sb.append(
                    Integer.toHexString(array[i] and 0xFF or 0x100).substring(
                        1,
                        3
                    )
                )
            }

            return sb.toString()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }

        return null
    }
}