package com.darkknightsds.draculadiscovery.random

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import com.darkknightsds.draculadiscovery.comic.Comic
import com.darkknightsds.draculadiscovery.R
import com.darkknightsds.draculadiscovery.comic.ComicStatus
import com.darkknightsds.draculadiscovery.detail.ComicDetailFragment
import kotlinx.android.synthetic.main.fragment_random_comic.*
import kotlinx.coroutines.*
import org.koin.android.ext.android.inject

class RandomComicFragment : Fragment(), View.OnClickListener {
    companion object {
        val parentJob = Job()
    }

    private val randomComicPresenter: RandomComicPresenter by inject()

    private val coroutineScope = CoroutineScope(Dispatchers.Main + parentJob)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_random_comic, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val marvelTypeface = ResourcesCompat.getFont(
            context!!,
            R.font.marvel_font
        )

        button_find_comic.typeface = marvelTypeface
        button_find_comic.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            button_find_comic -> {
                if (!isNetworkAvailable()) {
                    Toast.makeText(activity, getString(R.string.no_network_avail), Toast.LENGTH_SHORT).show()
                } else {
                    button_find_comic.text = ""
                    button_find_comic.isEnabled = false
                    progressBar_random.visibility = View.VISIBLE
                    coroutineScope.launch(Dispatchers.IO) {
                        val comic = randomComicPresenter.getRandomComic()
                        displayComicDetails(comic)
                    }
                }
            }
        }
    }

    private fun displayComicDetails(comic: Comic) {
        val fragment = ComicDetailFragment(comic, ComicStatus.RANDOM)

        activity?.runOnUiThread{
            progressBar_random.visibility = View.GONE
            button_find_comic.text = resources.getString(R.string.button_find_comic)
            button_find_comic.isEnabled = true
        }

        activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.frame_dracula_detail, fragment)?.commit()
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = context?.getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    override fun onDestroy() {
        super.onDestroy()

        coroutineScope.coroutineContext.cancelChildren()
    }
}
