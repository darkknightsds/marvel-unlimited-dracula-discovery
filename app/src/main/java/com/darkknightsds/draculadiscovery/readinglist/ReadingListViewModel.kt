package com.darkknightsds.draculadiscovery.readinglist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.darkknightsds.draculadiscovery.comic.Comic
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.core.KoinComponent
import org.koin.core.inject

class ReadingListViewModel: ViewModel(), KoinComponent {
    private val readingListPresenter: ReadingListPresenter by inject()

    var comics = MutableLiveData<List<Comic>>()

    fun readingListChanged() {
        viewModelScope.launch(Dispatchers.IO) {
            val list = readingListPresenter.getReadingList()
            withContext(Dispatchers.Main) {
                comics.value = list
            }
        }
    }
}