package com.darkknightsds.draculadiscovery.readinglist

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.darkknightsds.draculadiscovery.comic.Comic
import com.darkknightsds.draculadiscovery.R
import com.darkknightsds.draculadiscovery.comic.ComicStatus
import com.darkknightsds.draculadiscovery.detail.ComicDetailFragment
import kotlinx.android.synthetic.main.fragment_reading_list.*
import kotlinx.coroutines.*

import org.koin.android.ext.android.inject

class ReadingListFragment : Fragment() {
    companion object {
        val parentJob = Job()
    }

    private val readingListPresenter: ReadingListPresenter by inject()

    private val coroutineScope = CoroutineScope(Dispatchers.Main + parentJob)

    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    private lateinit var viewModel: ReadingListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container : ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reading_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val marvelTypeface = ResourcesCompat.getFont(
            context!!,
            R.font.marvel_font
        )

        textView_readingListTitle.isAllCaps = true
        textView_readingListTitle.typeface = marvelTypeface

        coroutineScope.launch(Dispatchers.IO) {
            val list = readingListPresenter.getReadingList()
            withContext(Dispatchers.Main) {
                displayList(list)
            }
        }

        viewModel = activity?.run {
            ViewModelProviders.of(this).get(ReadingListViewModel::class.java)
        } ?: throw Exception()

        coroutineScope.launch {
            viewModel.comics.observe(this@ReadingListFragment, Observer<List<Comic>> { list ->
                viewAdapter = ReadingListAdapter(list, comicSelected = this@ReadingListFragment::loadDetailFragment)
                recycler_readingList.adapter = viewAdapter
            })
        }
    }

    private fun displayList(data: List<Comic>) {
        viewManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        viewAdapter = ReadingListAdapter(data, comicSelected = this::loadDetailFragment)

        recycler_readingList.apply {
            setHasFixedSize(true)

            layoutManager = viewManager

            adapter = viewAdapter
        }
    }

    private fun loadDetailFragment(comicId: Int) {
        coroutineScope.launch(Dispatchers.IO) {
            val comic = readingListPresenter.getComicById(comicId)
            val comicDetailFragment = ComicDetailFragment(comic, ComicStatus.READING_LIST)
            activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.frame_dracula_detail, comicDetailFragment)?.commit()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        coroutineScope.coroutineContext.cancelChildren()
    }
}
