package com.darkknightsds.draculadiscovery.readinglist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.darkknightsds.draculadiscovery.R
import com.darkknightsds.draculadiscovery.comic.Comic
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.reading_list_item.view.*

class ReadingListAdapter(private val data: List<Comic>, val comicSelected: (comicId: Int) -> Unit) :
    RecyclerView.Adapter<ReadingListAdapter.ReadingListViewHolder>() {

    class ReadingListViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ReadingListViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.reading_list_item, parent, false) as View

        return ReadingListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ReadingListViewHolder, position: Int) {
        Picasso.get().load(data[position].thumbnail).into(holder.view.imageView_thumbnail)
        holder.view.imageView_thumbnail.setOnClickListener {
            comicSelected(data[position].id)
        }
    }

    override fun getItemCount() = data.size
}