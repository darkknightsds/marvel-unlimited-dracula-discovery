package com.darkknightsds.draculadiscovery.readinglist

import android.content.Context
import com.darkknightsds.draculadiscovery.comic.Comic
import com.darkknightsds.draculadiscovery.comic.ComicDatabase

class ReadingListPresenter(val context: Context) {
    fun getReadingList(): List<Comic> {
        return ComicDatabase.getDatabase(context).comicDao().getAll()
    }

    fun getComicById(id: Int): Comic {
        return ComicDatabase.getDatabase(context).comicDao().getById(id)
    }
}