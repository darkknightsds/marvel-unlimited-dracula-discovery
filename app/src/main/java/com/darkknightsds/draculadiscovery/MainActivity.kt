package com.darkknightsds.draculadiscovery

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import com.darkknightsds.draculadiscovery.detail.ComicDetailFragment
import com.darkknightsds.draculadiscovery.random.RandomComicFragment
import com.darkknightsds.draculadiscovery.readinglist.ReadingListFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val randomComicFragment = RandomComicFragment()
    private val readingListFragment = ReadingListFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar = findViewById<Toolbar>(R.id.app_toolbar)
        setSupportActionBar(toolbar)
        toolbar.changeToolbarFont()
        toolbar.setTitleTextColor(resources.getColor(R.color.colorAccent))
        toolbar.textAlignment = View.TEXT_ALIGNMENT_CENTER

        val marvelTypeface = ResourcesCompat.getFont(
            this,
            R.font.marvel_font
        )

        loadFragment(R.id.frame_dracula_random, randomComicFragment)
        loadFragment(R.id.frame_dracula_saved, readingListFragment)

        textView_placeholder.typeface = marvelTypeface
    }

    private fun loadFragment(view: Int, fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(view, fragment).commit()
    }

    private fun Toolbar.changeToolbarFont(){
        for (i in 0 until childCount) {
            val view = getChildAt(i)
            if (view is TextView && view.text == title) {
                view.typeface = ResourcesCompat.getFont(context,
                    R.font.marvel_font
                )
                view.textSize = 24f
                break
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        RandomComicFragment.parentJob.cancel()
        ReadingListFragment.parentJob.cancel()
        ComicDetailFragment.parentJob.cancel()
    }
}
