package com.darkknightsds.draculadiscovery.comic

import androidx.room.*

@Dao
interface ComicDao {
    @Query("SELECT * FROM comic ORDER BY time")
    fun getAll(): List<Comic>

    @Query("SELECT * FROM comic WHERE id IN (:id)")
    fun getById(id: Int): Comic

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(comic: Comic)

    @Delete
    fun delete(comic: Comic)
}
