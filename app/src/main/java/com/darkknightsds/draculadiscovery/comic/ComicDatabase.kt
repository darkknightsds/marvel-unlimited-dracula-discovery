package com.darkknightsds.draculadiscovery.comic

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = arrayOf(Comic::class), version = 1)
abstract class ComicDatabase : RoomDatabase() {
    abstract fun comicDao(): ComicDao

    companion object {
        private var INSTANCE: ComicDatabase? = null

        fun getDatabase(
            context: Context
        ): ComicDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE
                ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ComicDatabase::class.java,
                    "comics_db"
                )
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}