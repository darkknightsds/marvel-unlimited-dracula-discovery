package com.darkknightsds.draculadiscovery.comic

enum class ComicStatus {
    RANDOM, READING_LIST
}