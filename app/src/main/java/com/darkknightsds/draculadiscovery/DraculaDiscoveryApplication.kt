package com.darkknightsds.draculadiscovery

import android.app.Application
import com.darkknightsds.draculadiscovery.detail.ComicDetailPresenter
import com.darkknightsds.draculadiscovery.random.RandomComicPresenter
import com.darkknightsds.draculadiscovery.random.RandomComicService
import com.darkknightsds.draculadiscovery.readinglist.ReadingListPresenter
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.dsl.module

class DraculaDiscoveryApplication: Application() {
    private val appModule = module {
        single { RandomComicPresenter() }
        single { RandomComicService() }
        single { ComicDetailPresenter(applicationContext) }
        single { ReadingListPresenter(applicationContext) }
    }

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@DraculaDiscoveryApplication)
            modules(appModule)
        }
    }
}