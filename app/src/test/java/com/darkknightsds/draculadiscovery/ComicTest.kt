package com.darkknightsds.draculadiscovery

import com.darkknightsds.draculadiscovery.comic.Comic
import org.junit.Test

import org.junit.Assert.*

class ComicTest {
    private val id = 1
    private val title = "Hulk vs. Dracula"
    private val issueNumber = 1.00
    private val description = "This is a cool comic"
    private val pageCount = 123
    private val series = "Dracula Vs. Series"
    private val thumbnail = "www.test.com/123-thumb.jpg"
    private val image = "www.test.com/123.jpg"

    @Test
    fun randomComic_instantiates() {
        val comic = Comic(
            id,
            title,
//            issueNumber,
            description,
//            pageCount,
//            series,
            thumbnail,
            image
        )
        assertTrue(comic is Comic)
    }
}
